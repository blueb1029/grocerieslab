#ifndef GROCERYLIST_H
#define GROCERYLIST_H
#define MAXITEMS 10
#include "GroceryItemOrder.h"
#include <iostream>

class GroceryList
{
    private:
        GroceryItemOrder *gList;
        int listSize;
    public:
        GroceryList();
        void add(const GroceryItemOrder &);
        float getTotalCost() const;
        GroceryItemOrder & operator[](int);
        ~GroceryList();
};

#endif // GROCERYLIST_H
