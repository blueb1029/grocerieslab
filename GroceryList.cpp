#include "GroceryList.h"

using namespace std;

GroceryList::GroceryList()
{
    gList = new GroceryItemOrder[MAXITEMS];
    listSize = 0;
}

void GroceryList::add(const GroceryItemOrder &item)
{
    if(listSize < MAXITEMS) {
        delete &gList[listSize];
        gList[listSize] = item;
        listSize++;
    } else {
        cout << "List is full" << endl;
    }
}

float GroceryList::getTotalCost() const
{
    int i;
    float total = 0;
    for(i = 0; i < listSize; i++) {
        total += gList[i].getCost();
    }
    return total;
}

GroceryItemOrder & GroceryList::operator[](int index)
{
    return gList[index];
}

GroceryList::~GroceryList()
{
    delete gList;
}
