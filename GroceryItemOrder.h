#ifndef GROCERYITEMORDER_H
#define GROCERYITEMORDER_H
#include <string.h>
#define LIMIT 25

class GroceryItemOrder
{
    private:
        char *name;
        int quantity;
        float price;
    public:
        GroceryItemOrder() { name = '\0'; quantity = 0; price = 0;}
        GroceryItemOrder & operator=(const GroceryItemOrder&);
        GroceryItemOrder(const char*, int, float);
        float getCost() const;
        void setQuantity(int);
        void setItemName(const char*);
        void setPrice(float);
        char *getItemName() const;
        int getQuantity() const;
        float getPrice() const;
        ~GroceryItemOrder();
};

#endif // GROCERYITEMORDER_H
