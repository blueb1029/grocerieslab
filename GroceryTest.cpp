#include <iostream>
#include "GroceryList.h"
#include <string>

using namespace std;

void testGroceryItemOrder();
void testGroceryList();

int main()
{
    testGroceryItemOrder();
    testGroceryList();
    return 0;
}

void testGroceryItemOrder() {
    cout << "Default constructor:" << endl;
    GroceryItemOrder item1 = GroceryItemOrder("Pie", 1, 15.0);
    cout << "Name: " << item1.getItemName() << endl;
    cout << "Quantity: " << item1.getQuantity() << endl;
    cout << "Price: " << item1.getPrice() << endl;
    cout << "Cost: " << item1.getCost() << endl;
    cout << "Using Setters: " << endl;
    GroceryItemOrder item2 = GroceryItemOrder();
    item2.setItemName("Cookie");
    cout << "Name: " << item2.getItemName() << endl;
    item2.setQuantity(10);
    cout << "Quantity: " << item2.getQuantity() << endl;
    item2.setPrice(4.0);
    cout << "Price: " << item2.getPrice() << endl;
    cout << "Cost: " << item2.getCost() << endl;
}

void testGroceryList() {
    cout << "Grocery List:" << endl;
    GroceryList groceries = GroceryList();
    GroceryItemOrder item1 = GroceryItemOrder("Snickers", 3, 2.0);
    groceries.add(item1);
    cout << "Cost of item 1: " << groceries.operator[](0).getCost() << endl;
    GroceryItemOrder item2 = GroceryItemOrder("Butterfinger", 5, 1.5);
    groceries.add(item2);
    cout << "Cost of item 2: " << groceries.operator[](1).getCost() << endl;
    cout << "Total cost: " << groceries.getTotalCost() << endl;
}

