#include "GroceryItemOrder.h"

GroceryItemOrder::GroceryItemOrder(const char *n, int q, float p)
{
    name = new char[LIMIT];
    strcpy(name, n);
    quantity = q;
    price = p;
}

GroceryItemOrder & GroceryItemOrder::operator=(const GroceryItemOrder &)
{
    return *this;
}

float GroceryItemOrder::getCost() const
{
    return (quantity * price);
}

void GroceryItemOrder::setQuantity(int q)
{
    quantity = q;
}

void GroceryItemOrder::setItemName(const char*n)
{
    delete name;
    name = new char[LIMIT];
    strcpy(name, n);
}

void GroceryItemOrder::setPrice(float p)
{
    price = p;
}

char * GroceryItemOrder::getItemName() const
{
    return name;
}

int GroceryItemOrder::getQuantity() const
{
    return quantity;
}

float GroceryItemOrder::getPrice() const
{
    return price;
}

GroceryItemOrder::~GroceryItemOrder()
{
    delete name;
}
